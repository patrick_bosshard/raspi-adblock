# Raspi-Adblock #

Mit dem Raspberry Pi einen einfachen Adblocker einrichten. 

### Funktionsweise ###

Wenn ein Webbrowser eine Werbe-URL (Ad) aufrufen will, wird zuerst die URL mittels DNS aufgelöst. Erst in zweiten Schritt erfolgt
der Verbindungsaufbau zum Server.

Nun kann man auf dem Raspberry einen lokalen DNS-Server einrichten und die bekannten Werbe-URL (Ad) herausfiltern. Beim DNS-Lookup
liefert der Raspberry anstelle der IP des Werbe-Server einfach die IP seines eigenen Webserver.

Im Internet gibt es diverse Listen mit bekannten Werbe-Server. Mittels Shell-Skript updaten wir unsere Ad-Server-Liste.

### Setup ###

* lokaler DNS-Server Dnsmasq
* Nginx Webserver
* Update-Skript Ad-Server-Liste

#### DNS-Masq ####

wir verwenden Dnsmasq als lokaler DNS-Cache. Die Installation ist schnell und einfach.

```
#!shell
apt-get install dnsmasq
```

### Update-Skript Ad-Server-Liste ####

Mittels Shell-Skript laden wir uns eine Liste von bekannten Ad-Server und speichern es in einer Datei *adblock-list.conf*.

```
#!shell
#!/bin/sh

RASPI_IP=192.168.0.19
ADBLOCK_FILE=/home/pi/dnsmasq-adblock/adblock-list.conf

# Down the DNSmasq formatted ad block list
curl "http://pgl.yoyo.org/adservers/serverlist.php?hostformat=dnsmasq&showintro=0&mimetype=plaintext" | sed "s/127\.0\.0\.1/$RASPI_IP/" > $ADBLOCK_FILE

# Restart DNSmasq
sudo /etc/init.d/dnsmasq restart
```
In der Konfig-Datei von DNS-Masq (/etc/dnsmasq.conf) fügt man am Schluss folgende Zeile ein:

```
#!shell
conf-file=/home/pi/dnsmasq-adblock/adblock-list.conf
```

Das Shell-Skript noch gleich als Cron-Job einrichten. So wird die Liste alle zwei Stunden updated.
```
#!shell
0 */2 * * *  /home/pi/dnsmasq-adblock/update_adblock.sh > /dev/null 2>&1
```
#### Nginx Webserver ####

als Webserver kommt ein leichtgewichtiger Nginx Webserver zum Einsatz.

```
#!shell
apt-get install nginx
```
Die Standardinstallation ist ausreichend und benötigt keine weiteren Konfigurationen.

Im Log-File von Nginx kann man sehen, welche Infos an den Ad-Server geschickt worden wären.

```
#!shell
Log-File: /var/log/nginx/access.log
```